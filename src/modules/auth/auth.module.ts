import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configuration } from '../../utils/configuration';
import { EncryptionModule } from '../encryption/encryption.module';

import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { User } from './user.entity';

@Module({
    controllers: [AuthController],
    providers: [AuthService],
    imports: [
        TypeOrmModule.forFeature([User]),
        ConfigModule.forRoot({
            load: [configuration],
        }),
        PassportModule.register({ defaultStrategy: 'jwt' }),
        JwtModule.registerAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: (config: ConfigService) => {
                const { secret } = config.get('auth');

                return {
                    secret,
                };
            },
        }),
        EncryptionModule.registerAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: (config: ConfigService) => {
                const { algorithm, key, iv } = config.get('encryption');

                return {
                    algorithm,
                    iv: Buffer.from(iv, 'hex'),
                    key: Buffer.from(key, 'hex'),
                };
            },
        }),
    ],
})
export class AuthModule {}
