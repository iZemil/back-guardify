import { Controller, Post, Get, Body } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { ILoginBody, ISignUpBody } from './auth.interfaces';
import { AuthService } from './auth.service';
import { User } from './user.entity';

@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) {}

    @Get()
    public async getIndex() {
        return {
            success: true,
        };
    }

    @Get('users')
    public async getUsers() {
        return this.authService.getUsers();
    }

    @Post('sign-up')
    public async signUp(@Body() dto: ISignUpBody): Promise<User> {
        return this.authService.signUp(dto);
    }

    @Post('login')
    public async login(@Body() dto: ILoginBody): Promise<User & { token: string }> {
        return this.authService.login(dto);
    }

    @MessagePattern({ cmd: 'LOG_USER_ACTION' })
    public async logUserAction(data: any): Promise<boolean> {
        return this.authService.logUserAction(data);
    }
}
