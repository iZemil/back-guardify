import { BeforeInsert, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import * as bcrypt from 'bcrypt';

@Entity()
export class User {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({
        type: 'text',
        nullable: false,
    })
    public name: string;

    @Column({
        type: 'text',
        nullable: false,
    })
    public email: string;

    @Column({
        type: 'text',
        nullable: false,
    })
    public password: string;

    @BeforeInsert()
    private async hashPassword(): Promise<void> {
        const saltOrRounds = 10;
        const hashedPassword = await bcrypt.hash(this.password, saltOrRounds);
        this.password = hashedPassword;
    }
}
