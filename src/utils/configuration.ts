/* tslint:disable: no-magic-numbers */
import * as path from 'path';

const IS_PRODUCTION = process.env.NODE_ENV === 'production';

const isStringTrue = (envValue: string) => envValue === 'true';
const toInt = (envValue: string): number => parseInt(envValue, 10);

export const configuration = () => ({
    database: {
        database: process.env.DB_NAME,
        dropSchema: false,
        entities: IS_PRODUCTION
            ? [path.join(process.cwd(), '/build/modules/**/*.entity{.ts,.js}')]
            : [path.join(process.cwd(), '/src/modules/**/*.entity{.ts,.js}')],
        host: process.env.DB_HOST,
        logging: true,
        maxQueryExecutionTime: toInt(process.env.DB_MAX_QUERY_EXECUTION_TIME),
        password: process.env.DB_PASSWORD,
        port: toInt(process.env.DB_PORT),
        schema: process.env.DB_SCHEMA,
        ssl: isStringTrue(process.env.DB_SSL),
        synchronize: false,
        type: 'postgres',
        username: process.env.DB_USER,
    },
    api: {
        prefix: process.env.API_PREFIX,
    },
    auth: {
        cookie: {
            /** Максимальное время жизни куки (2 недели). */
            cookieMaxLifeTime: 3600000 * 7 * 24,
        },
        token: {
            name: 'jwtToken',
            /** Время сессии. */
            sessionLifeTime: 300 * 60 * 1000,
            /** Время продления сессии. */
            slidingExpiration: 30 * 60 * 1000,
        },
        secret: process.env.AUTH_SECRET ?? 'authSecr9A!>',
        /** Possible values: LOCAL, LDAP */
        mode: process.env.AUTH_MODE ?? 'LOCAL',
        ad: {
            url: process.env.AUTH_AD_URL ?? '',
            baseDN: process.env.AUTH_AD_BASEDN ?? '',
            username: process.env.AUTH_AD_USERNAME ?? '',
            password: process.env.AUTH_AD_PASSWORD ?? '',
        },
    },
    common: {
        /** Possible values: DEV, KSPD_TEST, KSPD */
        currentEnv: process.env.COMMON_CURRENT_ENV ?? 'DEV',
    },
    /**
     *
     * key, iv should be generated in this way
     * const iv = crypto.randomBytes(16).toString('hex');
     * const key = crypto.randomBytes(32).toString('hex');
     */
    encryption: {
        algorithm: 'aes-256-cbc',
        iv: '862fa92942bf520486dc796ac1a38fab',
        key: 'a173bba7d459e089e736d025f4a9363f5298cabd67d8dc18173f19106986b7bb',
    },
    host: process.env.HOST,
    mail: {
        auth: {
            password: process.env.SMTP_PASSWORD,
            userName: process.env.SMTP_USER,
        },
        from: process.env.SMTP_FROM,
        host: process.env.SMTP_HOST,
        port: toInt(process.env.SMTP_PORT),
        protocol: 'smtp',
        secure: isStringTrue(process.env.SMTP_SECURE),
    },
    ms: {
        port: toInt(process.env.MS_PORT),
    },
    server: {
        port: toInt(process.env.HTTP_PORT),
    },
    swagger: {
        prefix: 'api-doc',
    },
});
